package bsmch.TSP.Controllers;

import bsmch.TSP.Models.Request;
import bsmch.TSP.Models.City;
import bsmch.TSP.Services.TSP_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/BestTravel")
public class TSP_Controller {

  @Autowired TSP_Service tsp_service;

  @PostMapping("")
  public List<City> getAllLocations(@RequestBody Request request) {
    return this.tsp_service.getTheBestTravel(request);
  }
}
