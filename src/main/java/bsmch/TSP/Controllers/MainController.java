package bsmch.TSP.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {

  public String getMainPage() {
    return "./public/index.html";
  }
}
