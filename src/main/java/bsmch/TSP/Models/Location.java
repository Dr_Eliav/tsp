package bsmch.TSP.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {

  @JsonProperty private double x;

  @JsonProperty private double y;

  public Location(double x, double y) {
    this.setX(x);
    this.setY(y);
  }

  public Location(Location location) {
    this(location.x(), location.y());
  }

  private void setX(double x) {
    this.x = x;
  }

  private void setY(double y) {
    this.y = y;
  }

  public double x() {
    return this.x;
  }

  public double y() {
    return this.y;
  }

  @Override
  public String toString() {
    return "(" + x + ", " + y + ")";
  }
}
