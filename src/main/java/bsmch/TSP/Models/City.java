package bsmch.TSP.Models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class City {

  @JsonProperty private int cityNo;

  @JsonProperty private Location location;

  public City(int cityNo, Location location) {
    this.setCityNo(cityNo);
    this.setLocation(location);
  }

  private void setCityNo(int cityNo) {
    this.cityNo = cityNo;
  }

  private void setLocation(Location location) {
    this.location = new Location(location);
  }

  public Location location() {
    return this.location;
  }

  @Override
  public String toString() {
    return "{No=" + cityNo + ", " + location + '}';
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    } else if (!(other instanceof City)) {
      return false;
    }
    City city = (City) other;
    return cityNo == city.cityNo;
  }

  @Override
  public int hashCode() {
    return Objects.hash(cityNo, location);
  }
}
