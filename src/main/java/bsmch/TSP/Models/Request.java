package bsmch.TSP.Models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Request {

  @JsonProperty private int population;

  @JsonProperty private int generation;

  @JsonProperty private List<Location> locations;

  public List<Location> locations() {
    return this.locations;
  }

  public int populations() {
    return this.population;
  }

  public int generation() {
    return this.generation;
  }
}
