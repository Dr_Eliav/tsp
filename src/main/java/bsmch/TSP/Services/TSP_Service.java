package bsmch.TSP.Services;

import bsmch.TSP.Models.City;
import bsmch.TSP.Models.Request;
import java.util.Random;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TSP_Service {

  public static final Random random = new Random();

  public List<City> getTheBestTravel(Request request) {
    Population population = new Population(request.locations(), request.populations());
    population.calcFitness();

    for (int generation = 1; generation < request.generation(); generation++) {
      population.selection();
      population.crossover();
      population.mutation();
      population.calcFitness();
    }

    return population.bestChromosome().gens();
  }
}
