package bsmch.TSP.Services;

import bsmch.TSP.Models.City;
import bsmch.TSP.Models.Location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Chromosome implements Comparable<Chromosome> {

  private List<City> gens;
  private double fitness;

  public Chromosome(Chromosome chromosome) {
    this.gens = new ArrayList<>();
    this.gens.addAll(chromosome.gens);
    this.fitness = chromosome.fitness;
  }

  public Chromosome(List<Location> locations) {
    this.gens = new ArrayList<>();
    for (int index = 0; index < locations.size(); index++) {
      this.gens.add(new City(index + 1, locations.get(index)));
    }

    Collections.shuffle(this.gens);
  }

  public Chromosome(Chromosome parentA, Chromosome parentB) {
    Chromosome tempA = new Chromosome(parentA);
    Chromosome tempB = new Chromosome(parentB);


    final int crossPoint = TSP_Service.random.nextInt(tempA.gens.size());
    this.gens = new ArrayList<>();

    for (int index = 0; index < crossPoint; index++) {
      this.gens.add(tempA.gens.get(index));
    }

    tempB.gens.removeAll(this.gens);
    this.gens.addAll(tempB.gens);
  }

  public void mutation() {
    List<City> reverse = new ArrayList<>();
    int start = (int) Math.floor(this.gens.size() * (1 / 3.0));
    int end = (int) Math.ceil(this.gens.size() * (2 / 3.0));

    for (int index = start; index < end; index++) {
      reverse.add(this.gens.get(index));
    }

    Collections.reverse(reverse);
    for (int index = start, reverseIndex = 0; index < end; index++) {
      this.gens.set(index, reverse.get(reverseIndex++));
    }
  }

  public void calcFitness() {
    this.fitness = 0;

    for (int index = 0; index < this.gens.size(); index++) {
      double x1 = this.gens.get(index).location().x();
      double x2 = this.gens.get((index + 1) % this.gens.size()).location().x();
      double y1 = this.gens.get(index).location().y();
      double y2 = this.gens.get((index + 1) % this.gens.size()).location().y();

      this.fitness += Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }
  }

  public double fitness() {
    return this.fitness;
  }

  public List<City> gens() {
    return this.gens;
  }

  @Override
  public String toString() {
    return gens + ", fitness=" + fitness();
  }

  @Override
  public int compareTo(Chromosome chromosome) {
    return Double.compare(this.fitness(), chromosome.fitness());
  }
}
