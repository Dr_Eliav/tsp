package bsmch.TSP.Services;

import bsmch.TSP.Models.Location;

import java.util.ArrayList;
import java.util.List;

public class Population {

  private int population;
  private final List<Chromosome> chromosomes;
  private List<Chromosome> poolSelection;

  public Population(List<Location> locations, int population) {
    this.chromosomes = new ArrayList<>();
    this.population = population;

    for (int index = 0; index < population; index++) {
      this.chromosomes.add(new Chromosome(locations));
    }
  }

  public void selection() {
    poolSelection = new ArrayList<>();
    double chance;
    for (int index = 0; index < population; index++) {
      chance = population / this.chromosomes.get(index).fitness();

      for (int count = 0; count < chance; count++) {
        poolSelection.add(chromosomes.get(index));
      }
    }
  }

  public void crossover() {
    for (int index = 0; index < population; index++) {
      Chromosome partnerA = poolSelection.get(TSP_Service.random.nextInt(poolSelection.size()));
      Chromosome partnerB = poolSelection.get(TSP_Service.random.nextInt(poolSelection.size()));
      chromosomes.set(index, new Chromosome(partnerA, partnerB));
    }
  }

  public void mutation() {
    double MUTATION_PERCENTAGE = 0.25;
    for (int count = 0; count < MUTATION_PERCENTAGE * population; count++) {
      chromosomes.get(TSP_Service.random.nextInt(population)).mutation();
    }
  }

  public void calcFitness() {
    for (Chromosome chromosome : this.chromosomes) {
      chromosome.calcFitness();
    }
  }

  public Chromosome bestChromosome() {
    return this.chromosomes.stream().min(Chromosome::compareTo).get();
  }

  @Override
  public String toString() {
    String output = "chromosomes:\n";
    for (int index = 0; index < population; index++) {
      output += this.chromosomes.get(index).toString() + "\n";
    }
    return output;
  }
}
