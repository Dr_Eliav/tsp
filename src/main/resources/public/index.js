const canvasInput = document.getElementById('canvas-input');
const contextInput = canvasInput.getContext('2d');

let nodes = [];

const resize = () => {
    const width = (window.innerWidth) - 200;
    const height = (window.innerHeight) - 150;
    canvasInput.width = width;
    canvasInput.height = height;
};

window.onresize = resize;
resize();

const drawNode = (context, node) => {
    context.beginPath();
    context.fillStyle = node.fillStyle;
    context.arc(node.x, node.y, node.radius, 0, Math.PI * 2, true);
    context.strokeStyle = node.strokeStyle;
    context.stroke();
    context.fill();
};

const drawEdges = () => {
    for (let i = 0; i < nodes.length; i++) {
        let curr = nodes[i];
        let next = nodes[(i + 1) % nodes.length];
        contextInput.beginPath();
        contextInput.moveTo(curr.location.x, curr.location.y);
        contextInput.lineTo(next.location.x, next.location.y);
        contextInput.stroke();
    }
};

const click = (e) => {
    const { x, y } = e;
    const offsetX = -96;
    const offsetY = -6;
    if (x < (canvasInput.width - offsetX) && y < (canvasInput.height - offsetY)) {
        let node = {
            x,
            y,
        };
        node.x += offsetX;
        node.y += offsetY;
        nodes.push(node);
        drawNode(contextInput, { ...node, radius: 10, fillStyle: '#C0C0C0', strokeStyle: '#000000' });
    }
};

window.onclick = click;

const findBestTravel = async () => {
    reqBody = {
        population: document.getElementById("population").value,
        generation: document.getElementById("generation").value,
        locations: nodes,
    };

    try {
        const res = await fetch("http://localhost:8080/BestTravel", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(reqBody)
        });

        if (!res.ok) {
            throw new Error(res.status + " " + res.url);
        }

        nodes = await res.json();
    } catch (error) {
        alert(error)
    }

    drawEdges();
};

const clearCanvas = () => {
    nodes = [];
    contextInput.clearRect(0, 0, canvasInput.width, canvasInput.height);
};
